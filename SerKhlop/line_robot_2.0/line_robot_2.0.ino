int dt1 = 6 ;

int dt2 = 7 ;

int dt3 = 10 ;

int dt4 = 4;

int dt5 = 5 ;

int left_dir_pin = 12;
int leftSpeedPin = 3;


int right_dir_pin = 13;
int rightSpeedPin = 11;

int runSpeed = 200;

void setup() {
  pinMode (left_dir_pin, OUTPUT);
  pinMode (leftSpeedPin, OUTPUT);

  pinMode (right_dir_pin, OUTPUT);
  pinMode (rightSpeedPin, OUTPUT);

  byte a =6;
  byte b =7;
  byte c =10;
  byte d =4;
  byte e =5;


  pinMode(a,INPUT); 
  pinMode(b,INPUT);
  pinMode(c,INPUT);
  pinMode(d,INPUT);
  pinMode(e,INPUT);  
  // put your setup code here, to run once:

}

void loop() {

  
bool sens_1 = !digitalRead(dt1);
bool sens_2 = !digitalRead(dt2);
bool sens_3 = !digitalRead(dt3);
bool sens_4 = !digitalRead(dt4);
bool sens_5 = !digitalRead(dt5);
  
if(!sens_1 and !sens_2 and !sens_3 and !sens_4 and !sens_5) {
stp(); 
}

if(sens_3) {
fwd(); 
} 

if(sens_4) {
slwlft(); 
}  

if(sens_1) {
lft(); 
}

if(sens_2) {
slwrgt(); 
}

if(sens_5) {
rgt(); 
}

  
  // put your main code here, to run repeatedly:

}

void fwd() {

  digitalWrite (left_dir_pin, HIGH);
  analogWrite (leftSpeedPin, runSpeed);

  digitalWrite (right_dir_pin, HIGH);
  analogWrite (rightSpeedPin, runSpeed);

}

void bck() {
  
  digitalWrite (left_dir_pin, LOW);
  analogWrite (leftSpeedPin, runSpeed);

  digitalWrite (right_dir_pin, LOW);
  analogWrite (rightSpeedPin, runSpeed);
}

void stp() {
  digitalWrite (left_dir_pin, LOW);
  analogWrite (leftSpeedPin, LOW);

  digitalWrite (right_dir_pin, 0);
  analogWrite (rightSpeedPin, LOW);

}

 void slwrgt() {
  digitalWrite (right_dir_pin, HIGH);
  analogWrite (rightSpeedPin, runSpeed);

  digitalWrite (left_dir_pin, LOW);
  analogWrite (leftSpeedPin, LOW);
 }

 void slwlft() {
 digitalWrite (right_dir_pin, 0);
  analogWrite (rightSpeedPin, LOW);

   digitalWrite (left_dir_pin, HIGH);
  analogWrite (leftSpeedPin, runSpeed);
 }

 void lft() {
  digitalWrite (right_dir_pin, HIGH);
  analogWrite (rightSpeedPin, runSpeed);

  digitalWrite (left_dir_pin, LOW);
  analogWrite (leftSpeedPin, runSpeed);
 }

 void rgt() {
   digitalWrite (left_dir_pin, HIGH);
  analogWrite (leftSpeedPin, runSpeed);

   digitalWrite (right_dir_pin, LOW);
  analogWrite (rightSpeedPin, runSpeed);
 }
 
