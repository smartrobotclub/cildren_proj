#include <SPI.h>
#include <Ethernet.h>
  
// введите ниже MAC-адрес и IP-адрес вашего контроллера;
// IP-адрес будет зависеть от вашей локальной сети:
byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
IPAddress ip(192,168,0,20);
// инициализируем библиотеку Ethernet Server, указывая нужный порт
// (по умолчанию порт для HTTP – это «80»):
EthernetServer server(80);
 
  String readString = String(30);
  
  /*This is on digital pins 11, 12, and 13 
   * On both boards, pin 10 is used to select the W5100 and pin 4 for the SD card.
  */

// задаем контакт и начальное состояние для реле:
String relay1State = "Off";
String relay2State = "Off";
String relay3State = "Off";
String relay4State = "Off";

const int relay4 = 7;
const int relay3 = 6;
const int relay2 = 5;
const int relay1 = 8;

void setup() { 
// открываем последовательную коммуникацию на скорости 9600 бод:  
  Serial.begin(9600);
 // запускаем Ethernet-коммуникацию и сервер:  
  Ethernet.begin(mac, ip);
  server.begin();
  Serial.print("IP: ");
  Serial.println(Ethernet.localIP());
  // подготавливаем реле-модуль:
  pinMode(relay1,OUTPUT);
  digitalWrite(relay1, HIGH);
  pinMode(relay2,OUTPUT);
  digitalWrite(relay2, HIGH);
  pinMode(relay3,OUTPUT);
  digitalWrite(relay3, HIGH);
  pinMode(relay4,OUTPUT);
  digitalWrite(relay4, HIGH);
}

void loop() {
 
  EthernetClient client = server.available();
  if (client) {
    Serial.println("new client");
 
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
 
        if (readString.length() < 30){readString.concat(c);}Serial.print(c);
 
        if (c == '\n') {
 
// Показываем веб-страницу с кнопкой «вкл/выкл» для реле:         
          client.println("<!DOCTYPE HTML><html><head>");
          client.println("<h1>Antenna Switch VHF   <a href=\"/\">Refresh</a></h3>");
          client.println("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"></head><body>");
 
          client.println("<h3>Antenna 1 (80/40m): ");
 
        if(readString.indexOf("p=1") >=0){client.println("ON");digitalWrite(relay1,LOW);}
        else if(readString.indexOf("p=0") >=0){client.println("OFF");digitalWrite(relay1,HIGH);}
        else{client.println("OFF");digitalWrite(relay1,HIGH);}
 
          client.println("<h3>");
          client.print("<input type=button value='ON' onmousedown=location.href='/?p=1'> ");
          client.println(" <input type=button value='OFF' onmousedown=location.href='/?p=0'><br/><br/>");

           client.println("</html>");
           readString="";

          client.println("<h3>Antenna 2 (30/20m): ");
 
        if(readString.indexOf("p=1") >=0){client.println("ON");digitalWrite(relay2,LOW);}
        else if(readString.indexOf("p=0") >=0){client.println("OFF");digitalWrite(relay2,HIGH);}
        else{client.println("OFF");digitalWrite(relay2,HIGH);}
 
          client.println("<h3>");
          client.print("<input type=button value='ON' onmousedown=location.href='/?p=1'> ");
          client.println(" <input type=button value='OFF' onmousedown=location.href='/?p=0'><br/><br/>");
 
          client.println("</html>");
          readString="";

          client.println("<h3>Antenna 3 (20/15/10m): ");
 
        if(readString.indexOf("p=1") >=0){client.println("ON");digitalWrite(relay3,LOW);}
        else if(readString.indexOf("p=0") >=0){client.println("OFF");digitalWrite(relay3,HIGH);}
        else{client.println("OFF");digitalWrite(relay3,HIGH);}
 
          client.println("<h3>");
          client.print("<input type=button value='ON' onmousedown=location.href='/?p=1'> ");
          client.println(" <input type=button value='OFF' onmousedown=location.href='/?p=0'><br/><br/>");
 
          client.println("</html>");
          readString="";

          client.println("<h3>Antenna 4 (17/12m): ");
 
        if(readString.indexOf("p=1") >=0){client.println("ON");digitalWrite(relay4,LOW);}
        else if(readString.indexOf("p=0") >=0){client.println("OFF");digitalWrite(relay4,HIGH);}
        else{client.println("OFF");digitalWrite(relay4,HIGH);}
 
          client.println("<h3>");
          client.print("<input type=button value='ON' onmousedown=location.href='/?p=1'> ");
          client.println(" <input type=button value='OFF' onmousedown=location.href='/?p=0'><br/><br/>");
 
          client.println("</html>");
          readString="";
          
          break;

        }
      }
    }
 
    delay(1);
    client.stop();
    Serial.println("client disconnected");
   }
  }
