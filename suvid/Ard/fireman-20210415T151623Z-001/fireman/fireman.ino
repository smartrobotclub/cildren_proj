#include <Ultrasonic.h>
#include <Servo.h> 

#define flamePin      2 
#define pomp          10

#define leftDirPin    4 
#define leftSpeedPin  5
#define rightDirPin   7 
#define rightSpeedPin 6
#define buzzPin       11

Ultrasonic ultrasonic(12, 13);
 
int dist; // объявляем переменную для расстояния
 
int runSpeed = 150;

byte l = 90, i=1; //угол поворота сервы

Servo servo;

void setup(){

Serial.begin(9600); // отрываем порт для консоли
    
servo.attach(9);

pinMode(leftDirPin,   OUTPUT); 
pinMode(leftSpeedPin, OUTPUT ); 
pinMode(rightDirPin,  OUTPUT); 
pinMode(rightSpeedPin,OUTPUT ); 
pinMode(pomp,         OUTPUT ); 

digitalWrite ( pomp, 1);
delay (100);
digitalWrite ( pomp, 0);

forward(); 
}

void loop() 
{ 
if(l >170) i=-1; 
if(l == 10) i=1; 
l=l+i; 
servo.write(l); 

dist = ultrasonic.distanceRead();

Serial.println("Расстояние до объекта : ");
Serial.println(dist);
  
if(dist < 12){ wall();}

if( !digitalRead( flamePin ) ){ //работа - LOW, HIGH - нет игнала
  fire ();
  }
  
}

void fire () 
{ 
stop(); 
tone( buzzPin, 1000, 1000 ); 
delay (1000); 
servo.write(90);

delay (500); 

do
{
if(l<90)
  {
  turn_left();
  }
    else 
      {
      turn_right();
      }
}
while( digitalRead( flamePin ) );//пока не чработал датчик

stop ();

delay (1000);

while ( ultrasonic.distanceRead() > 10 ){
    if( !digitalRead( flamePin ) )//пока ечть чигнал
      {
         forward ();
      }else{
              stop();
              
              do
              {
              if(l >170) i=-1; 
              if(l == 10) i=1; 
              l=l+i; 
              servo.write(l); 
              delay(30);
              }
              while( digitalRead( flamePin ) );//пока не чработал датчик
              
              fire();
              //break;

            }
  }
  stop ();

   digitalWrite ( pomp, 1);
      
delay (5000);
digitalWrite ( pomp, 0);
//включить воду и лить до тех пор, пока  digitalRead( flamePin ) == HIGH
//потом развернутьчя и ехать дальше
//forward(); 
}

void forward() 
{ 
digitalWrite (leftDirPin, 0); 
digitalWrite (rightDirPin, 0);

analogWrite (leftSpeedPin , runSpeed); 
analogWrite (rightSpeedPin , runSpeed); 
}

void stop() 
{ 
analogWrite (leftSpeedPin , 0);//runSpeed); 
analogWrite (rightSpeedPin , 0);//runSpeed);

}

void turn_left()
{ 
digitalWrite (leftDirPin, 0); 
digitalWrite (rightDirPin, 0); 
analogWrite (leftSpeedPin , runSpeed); 
analogWrite (rightSpeedPin , runSpeed-60); 
delay (700); 
}

void turn_right()
{ 
digitalWrite (leftDirPin, 0); 
digitalWrite (rightDirPin, 0); 
analogWrite (leftSpeedPin , runSpeed-60); 
analogWrite (rightSpeedPin , runSpeed); 
delay (700); 
}

void wall(){
stop();
delay(1000);
back ();
delay(2000);
stop();
delay(1000);


digitalWrite (leftDirPin, 0); 
digitalWrite (rightDirPin, 0); 
analogWrite (leftSpeedPin ,0); 
analogWrite (rightSpeedPin , runSpeed); 

delay(1500);

forward();
 
}

void back()
{
digitalWrite (leftDirPin, 1); 
digitalWrite (rightDirPin, 1);

analogWrite (leftSpeedPin , 255-runSpeed);//runSpeed); 
analogWrite (rightSpeedPin , 255-runSpeed);//runSpeed); 
}
 
