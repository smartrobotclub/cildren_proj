#include <SoftwareSerial.h>


int gRxPin = 3;
int gTxPin = 2;
SoftwareSerial BTSerial(gRxPin, gTxPin);
int ldp = 4;//
int lds = 5;
int rdp = 7;
int rds = 6;
char b;
int runSpeed = 220;

void go(){
  analogWrite(ldp, HIGH);
  analogWrite(rdp, HIGH);
  analogWrite(lds, runSpeed);
  analogWrite(rds, runSpeed);
}

void stop(){
  analogWrite(lds, 0);
  analogWrite(rds, 0);
}

void goForward(){
  analogWrite(ldp, HIGH);
  analogWrite(rdp, HIGH);
  go();
}

void turnLeft(){
  analogWrite(lds, 100);
}

void turnRight(){
  
  analogWrite(rds, 100);

 }
 
void setup(){
   BTSerial.begin(9600);
 
 delay(500);
 Serial.begin (9600);
 pinMode(ldp, OUTPUT);
 pinMode(lds, OUTPUT);
 pinMode(rdp, OUTPUT);
 pinMode(rds, OUTPUT);


pinMode(10, INPUT_PULLUP);


  analogWrite(ldp, HIGH);
  analogWrite(rdp, HIGH);
}

  void loop() {
   if (BTSerial.available()) {
  b = BTSerial.read();
   }


  switch (b) {
    
  case 49:
 go();

    break;
    
  case 50:
   stop();
    break;
    
    case 51:
turnLeft();
delay(100);
stop();
    break;
    
    case 52:
 turnRight();
 delay(100);
 stop();
    break;
 
  }
  }
   

