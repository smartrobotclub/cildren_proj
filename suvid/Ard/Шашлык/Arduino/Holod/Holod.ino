
#define TERMIST_B 4300

#define VIN 5.0
#include <math.h>
void setup() {
  pinMode (7, OUTPUT);
  Serial.begin (9600);
}

void loop() {
  float voltage = analogRead(A0) * VIN / 1024.0;
  float r1 = voltage / (VIN - voltage);


  float temperature = 1. / ( 1. / (TERMIST_B) * log(r1) + 1. / (25. + 273.) ) - 273;
  Serial.println(temperature);
  delay(200);
  if (temperature >= 18) {
    digitalWrite(7, HIGH);
    Serial.println("FREESE");
  }
  else {
    digitalWrite(7, LOW);
    Serial.println("Off");
  }
}
