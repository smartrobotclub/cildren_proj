

#include <LiquidCrystal.h>
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
#include <math.h>
#define TERMIST_B 4300
#define VIN 5.0
void setup () 
{
  pinMode(9, OUTPUT);
  lcd.clear();
  //Serial.begin(9600);
  lcd.begin(16, 2);
}

void loop (){
 
  float voltage = analogRead(A0) * VIN / 1023.0;
  float r1 = voltage / (VIN - voltage);
  float temperature = 1./( 1./(TERMIST_B)*log(r1)+1./(25. + 273.) ) - 273;
  lcd.setCursor(6, 0);
  lcd.println(temperature); 
  lcd.setCursor(9, 0);
  lcd.print (" C   ");
  delay(200);
  lcd.clear();
  if (temperature>30)
  {
    digitalWrite(9,HIGH);
    tone (6,10,10000);
  }else {
     digitalWrite(9,LOW);
    }
  
}

