#include <NewPing.h> //подключение библиотеки
 
#define TRIGGER_PIN  8 //пин подключения контакта Trig
#define ECHO_PIN     9 //Пин подключения контакта Echo
#define MAX_DISTANCE 700 //максимально-измеряемое расстояние
 
//создание объекта дальномера 
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); 
 
void setup() {
  Serial.begin(9600); //инициализация Serial - порта
}
 
void loop() {
  delay(500); //задержка 500 мс
  int uS = sonar.ping(); //получение отклика датчика в микросекундах
  Serial.print("Ping: "); //Вывод сообщения
  Serial.print(sonar.convert_cm(uS)); //перевести микросекунды в сантиметры
  Serial.println("cm");
}
