#define leftDirPin 4
#define leftSpeedPin  5

#define rightDirPin  7
#define rightSpeedPin 6
void setup()
{ 
  pinMode(leftDirPin , OUTPUT);
  pinMode(leftSpeedPin ,OUTPUT );
  pinMode(rightDirPin , OUTPUT);
  pinMode(rightSpeedPin ,OUTPUT );
}

void loop() {
  // put your main code here, to run repeatedly:
digitalWrite (leftDirPin, HIGH);
  digitalWrite (rightDirPin, HIGH);
}
