#include <Servo.h>
Servo myservo;

#define leftDirPin 4
#define leftSpeedPin  5

#define rightDirPin  7
#define rightSpeedPin 6

int runSpeed = 10;
int trigPin = 8;
int echoPin = 13;

#define servoPin  9

int duration, cm;

void setup()
{
  pinMode(leftDirPin , OUTPUT);
  pinMode(leftSpeedPin , OUTPUT );
  pinMode(rightDirPin , OUTPUT);
  pinMode(rightSpeedPin , OUTPUT );

  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);

  myservo.attach(servoPin);
  myservo.write(90);
}

void loop()
{

  int r, l;
  distance();
  if (cm >= 15) {
    forward();
  } else {
    stopp();
    myservo.write(10);
    delay(1000);
    distance();
    r = cm;

    myservo.write(180);
    delay(1000);
    distance();
    l = cm;
    myservo.write(90);
    delay(1000);
    if (r > l) {
      right();
    } else if (r < l) {
      left();
    }
  }
}
/////////////////////////////////////////////////////
void forward()
{
  digitalWrite (leftDirPin, HIGH);
  digitalWrite (rightDirPin, HIGH);

  for (runSpeed > 0; runSpeed <= 255; runSpeed++)
  {
    delay(8);
    analogWrite (leftSpeedPin , runSpeed);
    analogWrite (rightSpeedPin , runSpeed);
  }

}
///////////////////////////////////////////////////
void right() {
  digitalWrite(leftDirPin, HIGH);
  digitalWrite(rightDirPin, LOW);
  analogWrite (leftSpeedPin , 100);
  analogWrite (rightSpeedPin , 100);

  delay(500);

  digitalWrite (leftSpeedPin, LOW);
  digitalWrite (rightSpeedPin , LOW);
}

//////////////////////////////////////////////

void left() {

  digitalWrite(leftDirPin, LOW);
  digitalWrite(rightDirPin, HIGH);

  analogWrite (leftSpeedPin , 100);
  analogWrite (rightSpeedPin , 100);

  delay(500);

  digitalWrite (leftSpeedPin, LOW);
  digitalWrite (rightSpeedPin , LOW);
}
///////////////////////////////////////////////////
void stopp()
{
  for (runSpeed < 255; runSpeed > 0; runSpeed--)
  {
    delay(8);
    analogWrite (leftSpeedPin , runSpeed);
    analogWrite (rightSpeedPin , runSpeed);
  }
}
//////////////////////////////////////////////////
void back()
{
  digitalWrite (leftDirPin, LOW);
  digitalWrite (rightDirPin, LOW);


  for (runSpeed > 0; runSpeed <= 255; runSpeed++)
  {
    delay(8);
    analogWrite (leftSpeedPin , runSpeed);
    analogWrite (rightSpeedPin , runSpeed);
  }
  digitalWrite (leftDirPin, LOW);
  digitalWrite (rightDirPin, LOW);


  for (runSpeed > 0; runSpeed <= 255; runSpeed++)
  {
    delay(8);
    analogWrite (leftSpeedPin , runSpeed);
    analogWrite (rightSpeedPin , runSpeed);
  }
}
/////////////////////////////////////////////////
void distance() {
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);

  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  duration = pulseIn(echoPin, HIGH);
  int right, left;

  cm = duration / 58 - 4;

  //  return(cm);
}
////////////////////////////////
