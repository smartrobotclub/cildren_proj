#define leftDirPin 4
#define leftSpeedPin  5

#define rightDirPin  7
#define rightSpeedPin 6

int runSpeed = 255;
void setup() {
  pinMode(leftDirPin , OUTPUT);
  pinMode(leftSpeedPin , OUTPUT );
  pinMode(rightDirPin , OUTPUT);
  pinMode(rightSpeedPin , OUTPUT );
}
void loop() {
  digitalWrite (leftDirPin, 1);
  digitalWrite (rightDirPin, 0);
  analogWrite (leftSpeedPin , runSpeed);
  analogWrite (rightSpeedPin , runSpeed);
}
