//Motor Pins
int ldp = 7; //Left motor direction
int lds = 6; //Left motor speed
int rdp = 4; //Right motor direction
int rds = 5; //Right motor speed

int runSpeed;

void setup() {
MotorSettings();
}
void loop() {
go();
delay(500);
stop();
delay(500);
back();
delay(1000);
turnLeft();
delay(500);
turnRight();
delay(500);

}
                          //Control
void MotorSettings(){
  runSpeed = 255;//Speed

  pinMode(ldp, OUTPUT);
  pinMode(lds, OUTPUT);
  pinMode(rdp, OUTPUT);
  pinMode(rds, OUTPUT);

  analogWrite(lds, HIGH);
  analogWrite(rds, HIGH);  
}
void go() {               //Go forward

  digitalWrite(ldp, HIGH);
  digitalWrite(rdp, HIGH);
  analogWrite(lds, runSpeed);
  analogWrite(rds, runSpeed);

}
void back() {             //Go back
  digitalWrite(ldp, LOW);
  digitalWrite(rdp, LOW);
  analogWrite(lds, runSpeed);
  analogWrite(rds, runSpeed);
}

void stop() {             //Stop
  analogWrite(lds, 0);
  analogWrite(rds, 0);
}
void turnLeft() {         //Turn left
  digitalWrite(ldp, HIGH);
  digitalWrite(rdp, HIGH);
  analogWrite(rds, runSpeed);
  analogWrite(lds, 15);
}

void turnRight() {        //Turn right
  digitalWrite(ldp, HIGH);
  digitalWrite(rdp, HIGH);
  analogWrite(lds, runSpeed);
  analogWrite(rds, 15);
}
                          //Sensors / Logic
void HSL(){               //HSL = Heat Sensor Logic
  
}