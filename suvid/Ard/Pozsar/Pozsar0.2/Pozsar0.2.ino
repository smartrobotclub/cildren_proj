int d = 4; //Debug //Change to 1 for LSL 2 for HSL 3 for Logic overall 4 for Motors/SL
//Motor Pins
int ldp = 7; //Left motor direction
int lds = 6; //Left motor speed
int rdp = 4; //Right motor direction
int rds = 5; //Right motor speed

bool SSS = 0;
int fire = 0;

int CCP = 8;

#include <Servo.h>
#include <NewPing.h>
Servo myservo;


bool S1 = 0;
bool S2 = 0;

bool wall = 0;
int wallpin = 2;
int wallcount = 0;

int Ang = 0;

int HP = 11;  // HEAT
bool HPI = 0;

int n;        //Distance Logic
int AngS  = 0;
bool Left;
bool Right;
int TD = 200; //Turn Delay

int runSpeed;
int runSped;

void setup() {
  runSped = 225;
  myservo.attach(9);
  MotorSettings();
  Serial.begin(9600);
  pinMode(HP, INPUT);
  pinMode(wallpin, INPUT);
}
void loop() {


  HSL();
  
}
void STEST() {
  myservo.write(0);
  delay(1000);
  myservo.write(90);
  delay(1000);
  myservo.write(180);
  delay(1000);
  myservo.write(90);
  delay(1000);
}
void MTEST() {
  go();
  delay(500);
  stop();
  delay(500);
  back();
  delay(1000);
  turnLeft();
  delay(500);
  turnRight();
  delay(500);

}
//Control
void MotorSettings() {
  runSpeed = 200;//Speed

  pinMode(ldp, OUTPUT);
  pinMode(lds, OUTPUT);
  pinMode(rdp, OUTPUT);
  pinMode(rds, OUTPUT);
  pinMode(CCP, OUTPUT);

  analogWrite(lds, HIGH);
  analogWrite(rds, HIGH);
}
void go() {               //Go forward

  digitalWrite(ldp, HIGH);
  digitalWrite(rdp, HIGH);
  analogWrite(lds, runSpeed);
  analogWrite(rds, runSpeed);
  if (d == 4) {
    Serial.println("Going Forward");
  }
}
void back() {             //Go back
  digitalWrite(ldp, LOW);
  digitalWrite(rdp, LOW);
  analogWrite(lds, runSpeed);
  analogWrite(rds, runSpeed);
  if (d == 4) {
    Serial.println("Going Back");
  }
}

void stop() {             //Stop
  analogWrite(lds, 0);
  analogWrite(rds, 0);
  if (d == 1) {
    Serial.println("STOP");
  }
}
void turnLeft() {         //Turn left
  digitalWrite(ldp, HIGH);
  digitalWrite(rdp, LOW);
  analogWrite(rds, runSpeed);
  analogWrite(lds, runSpeed);
  Left = 1;
  Right = 0;
  if (d == 4) {
    Serial.println("Turning Left");
  }
}

void turnRight() {        //Turn right
  digitalWrite(ldp, HIGH);
  digitalWrite(rdp, LOW);
  analogWrite(lds, runSpeed);
  analogWrite(rds, runSpeed);
  Left = 0;
  Right = 1;
  if (d == 4) {
    Serial.println("Turning Right");
  }
}
void stepLeft() {

  digitalWrite(ldp, HIGH);
  digitalWrite(rdp, HIGH);
  analogWrite(rds, runSped);
  analogWrite(lds, 15);
  Left = 1;
  Right = 0;
  if (d == 4) {
    Serial.println("Turning Left");
  }
  delay(200);
  stop();
}

void stepRight() {        //Turn right
  digitalWrite(ldp, HIGH);
  digitalWrite(rdp, HIGH);
  analogWrite(lds, runSped);
  analogWrite(rds, 15);
  Left = 0;
  Right = 1;
  if (d == 4) {
    Serial.println("Turning Right");
  }
  delay(200);
  stop();
}
void CCPT() {
  digitalWrite(CCP, HIGH);
  delay(1000);
  digitalWrite(CCP, LOW);
  delay(1000);
  fire = 0;
}
void SEARCH() {
  go();

  switch (AngS) {
    case 0:
      Ang = Ang + 10;
      if (Ang == 50) {
        AngS = 1;
      }
      break;
    case 1:
      Ang = Ang - 10;
      if (Ang == -50) {
        AngS = 0;
      }
      break;
    default:
      break;
  }
}
void SA() {
 
  if (Ang == 0){
    fire++;
  }
  if (Ang > 0 && Ang < 55) {
    stepLeft();
    Ang = Ang - 5;
    myservo.write(90 + Ang);
  } else if (Ang < 0 && Ang > -55) {
    stepRight();
    Ang = Ang + 5;
    myservo.write(90 + Ang);
  } else if (Ang == 0 || fire == 5) {
    myservo.write(90);
    CCPT();
  }

}
void patrol(){
  HPI = digitalRead(HP);
  if (HPI == 0) {
   if (!wall) {
     go();
    } else if ((Right == 0 && Left == 1) || (Right == 0 && Left == 0)) {
     stop();
     delay(100);
     turnRight();
     delay(TD);
    } else if (Right == 1 && Left == 0) {
     stop();
     delay(100);
     turnLeft();
     delay(TD);
    } else {
     stop();
    }
    }
  }

//Sensors / Logic
void HSL() {              //HSL = Heat Sensor Logic
  HPI = digitalRead(HP);
  wall = !digitalRead(wallpin);
  Serial.println(HPI);
  Serial.println(Ang);
  Serial.println(wall);
  delay(100);
  myservo.write(90 + Ang);
  delay(200);
  if (HPI == 0) {
    SEARCH();
    patrol();
  } else {
    Serial.println("FIRE");
    stop();
    SA();
  }
  if (Ang > 60 || Ang < -60) {
    myservo.write(90);
    Ang = 0;
  }
}
