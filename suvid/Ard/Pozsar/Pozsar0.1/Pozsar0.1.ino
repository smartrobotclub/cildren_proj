int d = 0; //Debug //Change to 1 for LSL 2 for HSL 3 for Logic overall
//Motor Pins
int ldp = 7; //Left motor direction
int lds = 6; //Left motor speed
int rdp = 4; //Right motor direction
int rds = 5; //Right motor speed

#include <Servo.h>
#include <NewPing.h>
Servo myservo; 

#define TRIGGER_PIN  2  
#define ECHO_PIN     3  
#define MAX_DISTANCE 45

NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE);

int serD = 3; //Distance Servo
int Ang = 0;

int n;        //Logic
int f;

int runSpeed;

void setup() {
myservo.attach(5);
MotorSettings();
Serial.begin(9600);
}
void loop() {
  
}
void STEST(){
  for(int i = 0;i++;i<=180){
  Ang = i;           
  myservo.write(Ang);  
  delay(15);
  }
  delay(100);
  for(int i = 180;i--;i>-1){
  Ang = i;           
  myservo.write(Ang);  
  delay(15);
  }
  delay(100);
  
}
void MTEST(){
  go();
delay(500);
stop();
delay(500);
back();
delay(1000);
turnLeft();
delay(500);
turnRight();
delay(500);

  }
                          //Control
void MotorSettings(){
  runSpeed = 255;//Speed

  pinMode(ldp, OUTPUT);
  pinMode(lds, OUTPUT);
  pinMode(rdp, OUTPUT);
  pinMode(rds, OUTPUT);

  analogWrite(lds, HIGH);
  analogWrite(rds, HIGH);  
}
void go() {               //Go forward

  digitalWrite(ldp, HIGH);
  digitalWrite(rdp, HIGH);
  analogWrite(lds, runSpeed);
  analogWrite(rds, runSpeed);

}
void back() {             //Go back
  digitalWrite(ldp, LOW);
  digitalWrite(rdp, LOW);
  analogWrite(lds, runSpeed);
  analogWrite(rds, runSpeed);
}

void stop() {             //Stop
  analogWrite(lds, 0);
  analogWrite(rds, 0);
}
void turnLeft() {         //Turn left
  digitalWrite(ldp, HIGH);
  digitalWrite(rdp, HIGH);
  analogWrite(rds, runSpeed);
  analogWrite(lds, 15);
}

void turnRight() {        //Turn right
  digitalWrite(ldp, HIGH);
  digitalWrite(rdp, HIGH);
  analogWrite(lds, runSpeed);
  analogWrite(rds, 15);
}
                          //Sensors / Logic
void HSL(){               //HSL = Heat Sensor Logic
  
}

void LSL(){               //DSL = Distance Sensor Logic
  delay(50);
  n = sonar.ping_cm();
  if (n != 0){
    f = n;
  }
  if (d == 1){
  Serial.print("Distance = ");
  Serial.println(f);
  }
}
