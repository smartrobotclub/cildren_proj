#include <SoftwareSerial.h>
#define left A2
#define middle A1
#define right A0
double timer ;
int ldp = 12;//
int lds = 10;
int rdp = 13;
int rds = 11;

int g = 0;

boolean l;
boolean r;

int runSpeed = 0;

boolean l_sense ;
boolean m_sense;
boolean r_sense;
//daln

#define TRIG A3
#define ECHO A4

#define MAX 200


void setup() {
  runSpeed = 80;
  Serial.begin (9600);
  pinMode(ldp, OUTPUT);
  pinMode(lds, OUTPUT);
  pinMode(rdp, OUTPUT);
  pinMode(rds, OUTPUT);

  pinMode(left, INPUT);
  pinMode(middle, INPUT);
  pinMode(right, INPUT);

  analogWrite(ldp, HIGH);
  analogWrite(rdp, HIGH);
}
void loop() {

  sens();
  //daln();
}
//daln

void go() {

  digitalWrite(ldp, HIGH);
  digitalWrite(rdp, HIGH);
  analogWrite(lds, runSpeed);
  analogWrite(rds, runSpeed);

}
void back() {
  digitalWrite(ldp, LOW);
  digitalWrite(rdp, LOW);
  analogWrite(lds, runSpeed);
  analogWrite(rds, runSpeed);
}

void stop() {
  analogWrite(lds, 0);
  analogWrite(rds, 0);
}
void turnLeft() {
  digitalWrite(ldp, HIGH);
  digitalWrite(rdp, HIGH);
  analogWrite(rds, runSpeed);
  analogWrite(lds, 15);
  l = 1;
  r = 0;
}

void turnRight() {
  digitalWrite(ldp, HIGH);
  digitalWrite(rdp, HIGH);
  analogWrite(lds, runSpeed);
  analogWrite(rds, 15);
  r = 1;
  l = 0;
}

void sens() {
  l_sense = digitalRead(left);
  m_sense = digitalRead(middle);
  r_sense = digitalRead(right);
  if (!l_sense && !r_sense && m_sense) {
    go();
    Serial.println("fwd");
  }
  if (l_sense && r_sense && m_sense) {
    if (l == 1 && r == 0) {
      turnLeft();
      l = 0;
    }
    if (l == 0 && r == 1) {
      turnRight();
      r = 0;
    }
    if (l_sense && r_sense && !m_sense) {
      go();
      Serial.println("fwd");
    }
    if (l_sense && r_sense && m_sense) {
      if (g == 0){
        turnLeft();
      delay(300);  
        g = 1;
      }else{
      turnRight();
      delay(300);
      g = 0;
      }
      
      
    }
  }
  if (!l_sense && !r_sense && !m_sense) {
    if (l == 1 && r == 0) {
      while (!l_sense && !r_sense && !m_sense) {
        l_sense = digitalRead(left);
        m_sense = digitalRead(middle);
        r_sense = digitalRead(right);
        turnLeft();
        if (!l_sense && r_sense && !m_sense) {
          delay(100);
          break;
        }
      }

    }
    if (l == 0 && r == 1) {
      while (!l_sense && !r_sense && !m_sense) {
        l_sense = digitalRead(left);
        m_sense = digitalRead(middle);
        r_sense = digitalRead(right);
        turnRight();
        if (l_sense && !r_sense && !m_sense) {
          delay(100);
          break;
        }
      }
    }
  }


  if (!l_sense && r_sense && m_sense) {
    turnLeft();
    l = 1;
    r = 0;
    Serial.println("lft");
  }
  else if (l_sense && !r_sense && m_sense) {
    turnRight();
    r = 1;
    l = 0;
    Serial.println("rgh");
  }
  else if (!l_sense && r_sense && !m_sense) {
    turnLeft();
    l = 1;
    r = 0;
    Serial.println("lft");
  }
  else if (l_sense && !r_sense && !m_sense) {
    turnRight();
    r = 1;
    l = 0;
    Serial.println("rgh");
  }

}
