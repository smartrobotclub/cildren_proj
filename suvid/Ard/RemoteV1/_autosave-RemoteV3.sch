EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x06_Female j1
U 1 1 6207A248
P 1850 4250
F 0 "j1" H 1878 4226 50  0000 L CNN
F 1 "BLUETOTH" H 1878 4135 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x06_P2.54mm_Vertical" H 1850 4250 50  0001 C CNN
F 3 "~" H 1850 4250 50  0001 C CNN
	1    1850 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 62080B6D
P 1650 3050
F 0 "R4" H 1720 3096 50  0000 L CNN
F 1 "10K" H 1720 3005 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 1580 3050 50  0001 C CNN
F 3 "~" H 1650 3050 50  0001 C CNN
	1    1650 3050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 620811C4
P 1450 1850
F 0 "R1" H 1520 1896 50  0000 L CNN
F 1 "10K" H 1520 1805 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 1380 1850 50  0001 C CNN
F 3 "~" H 1450 1850 50  0001 C CNN
	1    1450 1850
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 620817E7
P 1650 2200
F 0 "R2" V 1857 2200 50  0000 C CNN
F 1 "3.3K" V 1766 2200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 1580 2200 50  0001 C CNN
F 3 "~" H 1650 2200 50  0001 C CNN
	1    1650 2200
	0    1    -1   0   
$EndComp
$Comp
L Device:R R3
U 1 1 62081D63
P 1450 3050
F 0 "R3" H 1520 3096 50  0000 L CNN
F 1 "3.3K" H 1520 3005 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 1380 3050 50  0001 C CNN
F 3 "~" H 1450 3050 50  0001 C CNN
	1    1450 3050
	1    0    0    -1  
$EndComp
NoConn ~ 1650 4550
NoConn ~ 1650 4050
Wire Wire Line
	1450 2900 1350 2900
$Comp
L Connector:Conn_01x04_Female J2
U 1 1 620F08D1
P 3600 4400
F 0 "J2" H 3628 4376 50  0000 L CNN
F 1 "J1" H 3628 4285 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 3600 4400 50  0001 C CNN
F 3 "~" H 3600 4400 50  0001 C CNN
	1    3600 4400
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female J3
U 1 1 620F281F
P 3600 3800
F 0 "J3" H 3628 3776 50  0000 L CNN
F 1 "J2" H 3628 3685 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 3600 3800 50  0001 C CNN
F 3 "~" H 3600 3800 50  0001 C CNN
	1    3600 3800
	1    0    0    -1  
$EndComp
NoConn ~ 1900 2300
NoConn ~ 1900 2400
NoConn ~ 1900 2500
NoConn ~ 1900 2600
NoConn ~ 1900 2700
NoConn ~ 1900 2800
NoConn ~ 1900 3100
NoConn ~ 1900 3200
NoConn ~ 1900 3300
NoConn ~ 1900 3400
NoConn ~ 1900 3500
NoConn ~ 2900 3100
NoConn ~ 2900 3000
NoConn ~ 2900 2900
NoConn ~ 2900 2800
NoConn ~ 2900 2600
NoConn ~ 2900 2300
NoConn ~ 2900 2200
NoConn ~ 2500 1800
Wire Wire Line
	1750 3000 1900 3000
$Comp
L power:GND #PWR0101
U 1 1 621860D3
P 1350 2750
F 0 "#PWR0101" H 1350 2500 50  0001 C CNN
F 1 "GND" H 1355 2577 50  0000 C CNN
F 2 "" H 1350 2750 50  0001 C CNN
F 3 "" H 1350 2750 50  0001 C CNN
	1    1350 2750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1800 2200 1900 2200
Wire Wire Line
	1150 2000 1150 1700
Wire Wire Line
	1450 2000 1450 2200
Connection ~ 1450 2200
Wire Wire Line
	1450 2200 1500 2200
$Comp
L Transistor_BJT:MMBT3906 Q1
U 1 1 621C58E3
P 1250 2200
F 0 "Q1" H 1441 2154 50  0000 L CNN
F 1 "MMBT3906" H 1441 2245 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 1450 2125 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/2N3906-D.PDF" H 1250 2200 50  0001 L CNN
	1    1250 2200
	-1   0    0    1   
$EndComp
Wire Wire Line
	1650 2900 1900 2900
Wire Wire Line
	1350 2900 1350 2750
Wire Wire Line
	1450 3200 1650 3200
Wire Wire Line
	1450 3200 1350 3200
Wire Wire Line
	1350 3200 1350 4250
Wire Wire Line
	1350 4250 1650 4250
Connection ~ 1450 3200
Wire Wire Line
	1750 3000 1750 3300
Wire Wire Line
	1750 3300 1450 3300
Wire Wire Line
	1450 3300 1450 4150
Wire Wire Line
	1450 4150 1650 4150
Wire Wire Line
	1650 4350 1250 4350
Wire Wire Line
	1250 4350 1250 2900
Wire Wire Line
	1250 2900 1350 2900
Connection ~ 1350 2900
Wire Wire Line
	1150 1700 1450 1700
Wire Wire Line
	1650 4450 1150 4450
Wire Wire Line
	1150 4450 1150 2400
$Comp
L Connector:Conn_01x02_Male +1
U 1 1 6222CF2F
P 3900 1800
F 0 "+1" H 3872 1682 50  0000 R CNN
F 1 "-" H 3872 1773 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 3900 1800 50  0001 C CNN
F 3 "~" H 3900 1800 50  0001 C CNN
	1    3900 1800
	-1   0    0    1   
$EndComp
Wire Wire Line
	3400 3800 3150 3800
Wire Wire Line
	3400 4400 2500 4400
Wire Wire Line
	2500 4400 2500 3800
Wire Wire Line
	3400 4000 3400 4100
Wire Wire Line
	3400 4100 3700 4100
Wire Wire Line
	3700 4100 3700 3400
Wire Wire Line
	3700 3400 2900 3400
Wire Wire Line
	2900 3500 3400 3500
Wire Wire Line
	3400 3500 3400 3700
Wire Wire Line
	3400 4300 3400 4200
Wire Wire Line
	3400 4200 3800 4200
Wire Wire Line
	3800 4200 3800 3300
Wire Wire Line
	3800 3300 2900 3300
Wire Wire Line
	3400 4600 3400 4700
Wire Wire Line
	3400 4700 3900 4700
Wire Wire Line
	3900 4700 3900 3200
Wire Wire Line
	3900 3200 2900 3200
Wire Wire Line
	3400 3900 3250 3900
Wire Wire Line
	3250 3900 3250 1700
Wire Wire Line
	3400 4500 3250 4500
Wire Wire Line
	3250 4500 3250 3900
Connection ~ 3250 3900
Connection ~ 2500 3800
$Comp
L MCU_Module:Arduino_Nano_v3.x A1
U 1 1 62078C91
P 2400 2800
F 0 "A1" H 2400 1711 50  0000 C CNN
F 1 "Arduino_Nano_v3.x" H 2400 1620 50  0000 C CNN
F 2 "Module:Arduino_Nano" H 2400 2800 50  0001 C CIN
F 3 "http://www.mouser.com/pdfdocs/Gravitech_Arduino_Nano3_0.pdf" H 2400 2800 50  0001 C CNN
	1    2400 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 1800 2600 1700
Wire Wire Line
	2600 1700 3250 1700
Wire Wire Line
	2300 1600 3700 1600
Wire Wire Line
	3700 1600 3700 1700
Wire Wire Line
	2300 1600 2300 1800
Wire Wire Line
	2600 1700 1450 1700
Connection ~ 2600 1700
Connection ~ 1450 1700
Wire Wire Line
	3150 3800 3150 1800
Wire Wire Line
	3150 1800 3700 1800
Connection ~ 3150 3800
Wire Wire Line
	3150 3800 2500 3800
Wire Wire Line
	2400 3800 2500 3800
$EndSCHEMATC
