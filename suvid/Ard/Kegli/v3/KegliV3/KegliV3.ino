
#include <SoftwareSerial.h>
#define left A2
#define middle A1
#define right A0
double timer ;

boolean yes = 0;
int save = 0;

int distance;

int ldp = 12;//
int lds = 10;
int rdp = 13;
int rds = 11;

int runSpeed = 0;

boolean l_sense ;
boolean m_sense;
boolean r_sense;
//daln
#include<NewPing.h>;
#define TRIG A3
#define ECHO A4

#define MAX 200
NewPing sonar(TRIG, ECHO, MAX);

void setup(){
 Serial.begin (9600);
 pinMode(ldp, OUTPUT);
 pinMode(lds, OUTPUT);
 pinMode(rdp, OUTPUT);
 pinMode(rds, OUTPUT);

pinMode(left, INPUT);
pinMode(middle, INPUT);
pinMode(right, INPUT);

  analogWrite(ldp, HIGH);
  analogWrite(rdp, HIGH);
}
void loop() {  
 distance = sonar.ping_cm();

  if(distance <= 40 && distance > 5){
    stp();
    delay(300);
    distance = sonar.ping_cm();
   chek();
   if (distance <= 40 && distance > 5){
    go();
    timer = millis();
    //timer on
    delay(600);
    while(!digitalRead(left) && !digitalRead(middle) && !digitalRead(right) ){   
    }
    delay(70);
    timer = millis() - timer;
   /* if (timer < 1000){
      timer = save;
    }
    if (yes){
      save = timer;
      yes = 0;
    }
    */
    back();
    delay(timer);
    stp();
  }else{
    turnLeft();
    delay(500);
  }
  }
  turnRight();
  delay(200);
}
 void go(){
   runSpeed = 120;
  digitalWrite(ldp, HIGH);
  digitalWrite(rdp, HIGH);
  analogWrite(lds, runSpeed);
  analogWrite(rds, runSpeed);
}
void back(){
 runSpeed = 120;
  digitalWrite(ldp, LOW);
  digitalWrite(rdp, LOW);
  analogWrite(lds, runSpeed);
  analogWrite(rds, runSpeed);
}
void stp(){
  analogWrite(lds, 0);
  analogWrite(rds, 0);
}
void goForward(){  
  digitalWrite(ldp, HIGH);
  digitalWrite(rdp, HIGH);
  go();
}
void turnLeft(){
   runSpeed = 60;
  digitalWrite(ldp, LOW);
  analogWrite(lds, runSpeed);
  digitalWrite(rdp, HIGH);
  analogWrite(rds, runSpeed);
 
}
void turnRight(){
  runSpeed = 60;
  digitalWrite(ldp, HIGH);
  analogWrite(lds, runSpeed);
  digitalWrite(rdp, LOW);
  analogWrite(rds, runSpeed);
}
void chek(){
  distance = sonar.ping_cm();
  if (distance <= 40 && distance > 5){
  for(byte i = 0;i < 4;i++){
    if (save == 0 && distance > 40){
      turnLeft();
      delay(500 - 100 * i);
      save = 1;
    }
    if (save == 1 && distance > 40){
      turnRight();
      delay(500 - 100 * i);
      save = 0;
    }
  }
  }
  }
